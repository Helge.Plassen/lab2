package INF101.lab2;

import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;


public class Fridge implements IFridge {

    private int fridgeSize = 20;
    List<FridgeItem> fridgeContents;

    public Fridge() {
        fridgeContents = new ArrayList<FridgeItem>();
    }
    
    // Returns the number of items stored in the fridge.
    public int nItemsInFridge() {

        return this.fridgeContents.size();
    }

    // Returns the maximum number of items one can store in the fridge.
    public int totalSize() {

        return this.fridgeSize;
    }

    // Puts a food-item inside the fridge.
    public boolean placeIn(FridgeItem item){
        if (this.nItemsInFridge() == this.totalSize()){
            return false;
        }

        else {
            fridgeContents.add(item);
            return true;
        }

    }

    // Remove an item from the fridge.
    public void takeOut(FridgeItem item) {
        if (this.fridgeContents.contains(item)) {
            this.fridgeContents.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }
    }

    // Remove all expired foods from the fridge.
    public List<FridgeItem> removeExpiredFood() {

        List<FridgeItem> expiredFood = new ArrayList<FridgeItem>();
        
        /* Go through all the contents of the fridge... */
        for (FridgeItem item : this.fridgeContents) {
            
            /* Find all the expired food... */
            if (item.hasExpired()) {;
                expiredFood.add(item);
            }

        }

        /* And remove them from the fridge. */
        for (FridgeItem expiredItem : expiredFood) {
            this.fridgeContents.remove(expiredItem);
        }

        return expiredFood;
    }

    // Clean out the fridge.
    public void emptyFridge() {
        this.fridgeContents.clear();
        
    }
}
